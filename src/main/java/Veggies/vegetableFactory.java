/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Veggies;

/**
 *
 * @author grant
 */
public class vegetableFactory {
    
    private static vegetableFactory factory;
    
    private vegetableFactory(){}
    
    public static  vegetableFactory makeFactory(){
        
        if (factory==null){
            factory = new vegetableFactory();
        }
        return factory;
    }
    
    public Veggies grabVeggie(String name, Boolean ripe){
        
        if(name.equalsIgnoreCase("carrot")){
           if(ripe){
               return new Carrot("orange",2);
           } else{
               return new Carrot("yellow",1);
           } 
            
        }else if (name.equalsIgnoreCase("beet")){
           if(ripe){
               return new beet("red",3);
           } else{
               return new beet("green",1);
           }  
        }        
     return null;   
    }
    
}
